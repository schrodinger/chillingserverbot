// Load up the discord.js library
const Discord = require("discord.js");
const logger = require('winston');
const { Pool, Client } = require('pg');
const qfgets = require('qfgets');

//configure Logger
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
		colorize: true
});
logger.level = 'debug';

// Database Connect
const pool = new Pool({
  user: 'DiscordBot',
  host: '127.0.0.1',
  database: 'Discord',
  password: 'Disc0rdB0t',
  port: 5432,
})

/*
pool.query('SELECT NOW()', (err, res) => {
  logger.debug(err, res)
  pool.end()
})
*/

//Database Tests
// And set Initial Database Variables
pool.query('SELECT id from insults.phrases order by id desc limit 1', (err, res) => {
  if (err) {
    logger.debub(err.stack);
  } else {
    var LastInsultID = res.rows[0].id
    logger.debug(LastInsultID);
  }
})

//Configure Variables for Later

function filterProhibitedString(string) {
    if ( string.indexOf("<@95497136338108416>") !== -1 ) {
        return string.replace(/<@95497136338108416>/g, '@Blackbox')
    } else {
        return string;
    }
}

//Confgiure funtions for later
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

// This is your client. Some people call it `bot`, some people call it `self`, 
// some might call it `cootchie`. Either way, when you see `client.something`, or `bot.something`,
// this is what we're refering to. Your client.
const client = new Discord.Client();

// Here we load the config.json file that contains our token and our prefix values. 
const config = require("./config.json");
// config.token contains the bot's token
// config.prefix contains the message prefix.

client.on("ready", () => {
	// This event will run if the bot starts, and logs in, successfully.
	console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
	// Example of changing the bot's playing game to something useful. `client.user` is what the
	// docs refer to as the "ClientUser".
	client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("guildCreate", guild => {
	// This event triggers when the bot joins a guild.
	console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
	client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("guildDelete", guild => {
	// this event triggers when the bot is removed from a guild.
	console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
	client.user.setActivity(`Serving ${client.guilds.size} servers`);
});


client.on("message", async message => {
	// This event will run on every single message received, from any channel or DM.
	
    if (message.content.toLowerCase().indexOf("cdn.nekos.life/neko") !== -1) {
        message.react(message.guild.emojis.get('443189567735201805'));
        //message.delete() //delete the cat girl
        //message.channel.send("https://cdn.discordapp.com/attachments/371339323901214723/443186306655322122/6e6701fe473ff9d9a003b3035afff84a.png") //bring in the girl cat
    }

	// It's good practice to ignore other bots. This also makes your bot ignore itself
	// and not get into a spam loop (we call that "botception").
	if(message.author.bot) return;
	// Also good practice to ignore any message that does not start with our prefix, 
	// which is set in the configuration file.
    if (message.content.toLowerCase().indexOf("<@95497136338108416>") !== -1) {
        var originalMessage= message.content
        message.channel.send(`Now how would you like it if I just went around @ing you all day <@${message.author.id}>`);
        message.channel.send("I've cleaned your message and I will resend it for you");
        message.delete()
        message.channel.send(filterProhibitedString(originalMessage));
        return;
    } else if (message.content.toLowerCase() === "help") {
        message.channel.send("the prefix for this bot is\n+command");
    } else if (message.content.toLowerCase().indexOf("?girlcat") !== -1) {
        message.channel.send("https://cdn.discordapp.com/attachments/371339323901214723/443186306655322122/6e6701fe473ff9d9a003b3035afff84a.png");
    } else if (message.content.toLowerCase().indexOf("?cat") !== -1) {
        var sendMessage = message.content.slice(message.content.indexOf("?")).split(/ +/g).shift().replace(/\?/ig,``)
        sendMessage += " "
        if (sendMessage.toLowerCase().indexOf("catgirl") !== -1) {
            return;
        } else if (sendMessage.toLowerCase().indexOf("cat ") !==-1 ) {
            return;
        }
        message.channel.send(`What the fuck is a ${sendMessage}and why do you think I would know what to do with this.`)
        return;
    } else if (message.content.slice(message.content.indexOf("?")).split(/ +/g).shift().toLowerCase().indexOf("girl") !== -1) {
        var sendMessage = message.content.slice(message.content.indexOf("?")).split(/ +/g).shift().replace(/\?/ig,``)
        message.channel.send(`Now you're searching for ${sendMessage}. How horney are you?`)
        return;
    } else if (message.content.toLowerCase().indexOf("@someone") !== -1) {
        var people = [];
        message.channel.members.forEach(function(key,value) {logger.debug(value);if (value !== "95497136338108416") {people.push(value)};});
        person = people[getRandomInt(people.length)]
        message.channel.send(message.content.replace(/@someone/ig, `<@${person}>`))
        message.delete();
        return;
    } else if (message.content.toLowerCase().indexOf("?marsha") !== -1) {
        message.channel.send("https://cdn.discordapp.com/attachments/394896939470159873/452871194685997057/RPG-42.png")
        return;
    } else if (message.content.indexOf(config.prefix) == 0 ) {
        logger.debug(message.content);
        const args = message.content.slice(message.content.indexOf(config.prefix)).slice(config.prefix.length).trim().split(/ +/g);
        logger.debug(args);
        const command = args.shift().toLowerCase();
        logger.debug(command);
        
        // Let's go with a few common example commands! Feel free to delete or change those.
        
        if(command === "ping") { //Am I still there?
            message.channel.send("FUCKING PONG");
        }

        else if(command === "say") { //I'll Say whatever you want me to say
            // makes the bot say something and delete the message. As an example, it's open to anyone to use. 
            // To get the "message" itself we join the `args` back into a string with spaces: 
            var sayMessage = args.join(" ");
            sayMessage=`${message.author} asked me to say: ${sayMessage}`
            // Then we delete the command message (sneaky, right?). The catch just ignores the error with a cute smiley thing.
            message.delete().catch(O_o=>{}); 
            // And we get the bot to say the thing: 
            message.channel.send(filterProhibitedString(sayMessage));
        }
        else if(command === "addinsult") { //This will add an insult to the bots bank
            var insultPerson = "false"
            const insultSubmiter = message.author.id
            var insultMessage = args.join(" ");
            if (insultMessage.search(/<@![0-9]*>/g) !== -1) {
                message.channel.send("I will not include/@ specific people in my insults. To use the targets name use +person in their place");
                return;
            };
            insultMessage= insultMessage.replace(/\'/g,"\'\'")
            logger.debug(`insultMessage=${insultMessage}`);
            if (message.content.toLowerCase().search(/\+person/g) !== -1) {
                insultPerson = "true"
            };
            pool.query('SELECT id from insults.phrases order by id desc limit 1', (err, res) => {
                if (err) {
                    logger.debub(err.stack);
                    return;
                } else {
                    var LastInsultID = res.rows[0].id
                    var NextInsultID = LastInsultID+1
                    logger.debug(`${LastInsultID}->${NextInsultID}`);
                };
                pool.query("Insert INTO insults.phrases (id, submittedby, hasname, message) VALUES ('"+ NextInsultID +"', '"+ insultSubmiter +"', '"+ insultPerson +"', '"+ insultMessage + "');", (err, res)  => {
                //pool.query("Insert INTO insults.phrases (id, submittedby, hasname, message) VALUES ('1','Test','false','You are so bad you are worse than this insult');", (err, res)  => {
                    if (err) {
                        logger.debug(err.stack);
                        return;
                    } else {
                        logger.debug(res.rows[0])
                        return;
                    }
                })
            })
        }
        else if(command === "insultsomeone") { //I'll Insult a Random Person
            pool.query('SELECT id from insults.phrases order by id desc limit 1', (err, res) => {
                if (err) {
                    logger.debub(err.stack);
                    return;
                } else {
                    var LastInsultID = res.rows[0].id
                    var NextInsultID = LastInsultID+1
                    logger.debug(`${LastInsultID}->${NextInsultID}`);
                    var insultMessageNumber= Math.floor(Math.random() * NextInsultID);
                    logger.debug(`MessageNumber=${insultMessageNumber}`);
                };
                pool.query("SELECT message, hasname, submittedby FROM insults.phrases WHERE id = '" + insultMessageNumber  + "';", (err,res) => {
                    //logger.debug("here");
                    if (err) {
                        logger.debug(err.stack);
                        return;
                        //logger.debug("fail");
                    } else {
                        //logger.debug("success");
                        insultMessage= res.rows[0].message;
                        insultHasName= res.rows[0].hasname;
                        insultComposer= res.rows[0].submittedby;
                        insultER=message.author;
                        var people = [];
                        message.channel.members.forEach(function(key,value) {if (value !== "95497136338108416") { if ( value !== message.author.id ) {people.push(value)};};});
                        person = people[getRandomInt(people.length)]
                        if ( insultHasName === "true") {
                            insultMessage= insultMessage.replace(/\+person/ig, `<@${person}>`)
                        }
                        message.delete().catch(O_o=>{});
                        var sayMessage = `<@${person}>, ${insultER} wanted me to tell you:\n${insultMessage}`
                        message.channel.send(filterProhibitedString(sayMessage))
                        return;
                    }
                })
            })
        }
        else if(command === "insultme") { //I'll insult you
            pool.query('SELECT id from insults.phrases order by id desc limit 1', (err, res) => {
                if (err) {
                    logger.debub(err.stack);
                    return;
                } else {
                    var LastInsultID = res.rows[0].id
                    var NextInsultID = LastInsultID+1
                    logger.debug(`${LastInsultID}->${NextInsultID}`);
                    var insultMessageNumber= Math.floor(Math.random() * NextInsultID);
                    logger.debug(`MessageNumber=${insultMessageNumber}`);
                };
                pool.query("SELECT message, hasname, submittedby FROM insults.phrases WHERE id = '" + insultMessageNumber  + "';", (err,res) => {
                    //logger.debug("here");
                    if (err) {
                        logger.debug(err.stack);
                        return;
                    } else {
                        insultMessage= res.rows[0].message;
                        insultHasName= res.rows[0].hasname;
                        insultComposer= res.rows[0].submittedby;
                        insultER=message.author;
                        if ( insultHasName === "true") {
                            insultMessage= insultMessage.replace(/\+person/ig, `<@${person}>`)
                        }
                        var sayMessage = `<@${insultComposer}> wrote to me this;\n ${insultMessage}\n I thought of you, ${message.author}, when they wrote this`
                        message.channel.send(filterProhibitedString(sayMessage))
                    }
                })
            })
        }
        else if(command === "listmyinsults") { //List the insults you created
            if(args[0] > 0 ) {
                var SearchID = args[0]
            } else {
                var SearchID = 0
            }
            message.channel.send(args[0])
            pool.query("SELECT * from insults.phrases where id >= '" + SearchID + "' and submittedby = '" + message.author.id + "' order by id", (err, res) => {
                if (err) {
                    message.channel.send(`Huh.... I don't know what to tell you but I errored out`);
                } else if (res.rowCount > 0 ) {
                    var sayMessage = ""
                    res.rows.forEach(function(row) {
                        sayMessage += `id=${row.id}, message=${row.message}\n`
                        if ( sayMessage.length > 1500) {
                            message.author.send(filterProhibitedString(sayMessage))
                            sayMessage = ""
                        }
                    })
                    message.author.send(filterProhibitedString(sayMessage))
                } else {
                    message.channel.send("You little fuckwit. You haven't written any messages or you gave me a bad number so I have nothing to send you. I only list insults that you have sent")
                }
            })
        }
        else if(command === "listinsults") { //List all insults, or insult @ID
            if(args[0] > 0 ) {
                var SearchID = args[0]
            } else {
                var SearchID = 0
            }
            message.channel.send(args[0])
            pool.query("SELECT * from insults.phrases where id >= '" + SearchID + "' order by id ASC", (err, res) => {
                if (err) {
                    message.channel.send(`Huh.... I don't know what to tell you but I errored out`);
                } else if (res.rowCount > 0 ) {
                    var sayMessage = " "
                    res.rows.forEach(function(row) {
                        sayMessage += `id=${row.id}, message=${row.message}\n`
                        if ( sayMessage.length > 1500) {
                            message.author.send(filterProhibitedString(sayMessage))
                            sayMessage = " "
                        }
                    })
                    message.author.send(filterProhibitedString(sayMessage))
                } else {
                    message.channel.send("You little fuckwit. You haven't written any messages or you gave me a bad number so I have nothing to send you. I only list insults that you have sent")
                }
            })
        }
        else if(command === "editinsult") { //Edit an insult you created
            if(args[0] > 0 ) {
                var SearchID = args.shift()
            } else {
                var SearchID = 0
            }
            pool.query("Select submittedby from insults.phrases where id = '" + SearchID + "' limit 1", (err,res) => {
                if ( message.author.id === res.rows[0].submittedby ) {
                    var sayMessage = args.join(" ")
                    pool.query("Update insults.phrases set message = '" + sayMessage + "' where id = '" + SearchID + "' ")
                    message.channel.send(`I have updated the message to read \n\n ${sayMessage}`)
                } else {
                    message.channel.send("You can only edit a message you submitted")
                }
            })
        }
        else if(command === "addgame") { //add a game to my database
            var gameName = args.join(" ").toLowerCase()
            pool.query("Insert into games.storage (gamename,players) VALUES('" + gameName + "', ' ');", (err, res) => { 
                if (err) {
                    message.channel.send(`Aww fuck somebody messed up here's what I sent \n\n ${gameName} \n\n and here is what I saw from the database \n\n ${err.stack} \n\n My guess that game is already in there ::shrug::`);
                    //logger.debug("fail");
                } else {
                    message.channel.send(`ALRIGHT! I have added ${gameName} to my list. Thank you!`)
                }
            })
        }
        else if(command === "whoplays") { //Ask me who plays a game
            var gameName = args.join(" ").toLowerCase()
            pool.query("select players from games.storage where gamename = '" + gameName + "';", (err,res) => {
                if (err) {
                    logger.deubg(err.stack)
                    message.channel.send("Damn, I don't think that went right. The database might be down?")
                } else {
                    if (res.rowCount > 0 ) {                    
                        message.channel.send(`The following players play ${gameName} ${res.rows[0].players} I hope you don't mind I called all of them for you :shrug:`);
                    } else {
                        message.channel.send("I didn't find that game.\nUse +listgames to see what I know about.\nUse +addgame to add a missing game")
                    }
                }
            })
        }
        else if(command === "iplay") { //Tell me the game you play. I'll add it if it doesn't exist
            var gameName = args.join(" ").toLowerCase()
            pool.query("select players from games.storage where gamename = '" + gameName + "';", (err,res) => {
                if (err) {
                    message.channel.send("Something is FUBAR and I failed to search the database")
                } else {
                    if (res.rowCount > 0 ) {
                        var myself = message.author;
                        var players = res.rows[0].players;
                        if ( players.search(myself) !== -1 ) {
                            message.channel.send("from what I could tell you already told me you play this game");
                        } else {
                            players += ` ${myself}`
                            pool.query("Update games.storage set players = '" + players + "' where gamename = '" + gameName + "';"), (err,res) => {
                                if (err) {
                                    message.channel.send(`I HAVE FAILED YOU, please forgive me for I could not add you to the game because \n\n ${err.stack}`)
                                    logger.debug(err.stack);
                                    return;
                                } else {
                                    message.channel.send(`I Have added you to ${gameName}`)
                                    return;
                                }
                            }
                        }
                    } else {
                        pool.query("Insert into games.storage (gamename,players) VALUES('" + gameName + "', ' " + myself + "');", (err, res) => { 
                            if (err) {
                                message.channel.send(`Aww fuck somebody messed up here's what I sent \n\n ${gameName} \n\n and here is what I saw from the database \n\n ${err.stack} \n\n My guess that game is already in there ::shrug::`);
                            } else {
                                message.channel.send(`ALRIGHT! I have added ${gameName} to my list and you to it. Thank you!`)
                            }
                        })
                    }
                }
            })
        }
        else if(command === "listgames") { //List the games I know you play
            pool.query('Select gamename from games.storage;', (err,res) => {
                if (err) {
                    logger.debug(err.stack);
                    return;
                    //logger.debug("fail");
                } else {
                    var sayMessage="My Current Game list includes\n"
                    res.rows.forEach(function(row) {
                        sayMessage += `${row.gamename}\n`
                    })
                    message.channel.send(filterProhibitedString(sayMessage))
                    return;
                }
            })
        }
        else { 
            message.channel.send(`I mean what the fuck am I supposed to do with ${command}`)
        };
	};
	
});

client.login(config.token);